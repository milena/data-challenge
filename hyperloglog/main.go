package main

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"time"

	kafka "github.com/segmentio/kafka-go"
)

type Message struct {
	Id        string `json:"uid"`
	Timestamp int    `json:"ts"`
}

type OutputMessage struct {
	Timestamp int    `json:"ts"`
	Interval  string `json:"interval"`
	Hits      int    `json:"hits"`
}

type Interval int

const (
	Minute Interval = 0
	Hour            = 1
	Day             = 2
	Week            = 3
)
const numOfIntervals = 4
const numOfBits = 12
const numOfNibbles = numOfBits / 4
const numOfBuckets = 1 << numOfBits

func (i Interval) String() string {
	switch i {
	case Minute:
		return "Minute"
	case Hour:
		return "Hour"
	case Day:
		return "Day"
	case Week:
		return "Week"
	}
	return "unknown"
}

func main() {

	if numOfBits%4 > 0 {
		panic("Bits undivisible by 4 are unsupported (for now)")
	}

	fmt.Printf("running...\n")

	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{"localhost:9093"},
		GroupID:  "consumer-group-id-2",
		Topic:    "data-challenge",
		MinBytes: 10,   // 10B
		MaxBytes: 10e6, // 10MB
	})

	// Commented parts of testing with stdin
	// reader := bufio.NewReader(os.Stdin)

	var previousMinute int
	var regs [numOfIntervals][numOfBuckets]int
	var count [numOfIntervals]int
	var alphaM float64 = 0.7213 / (1 + 1.079/numOfBuckets)

	for {
		rawMessage, err := r.ReadMessage(context.Background())
		// rawMessage, err := reader.ReadString('\n')

		if err != nil {
			break
		}

		var message Message
		json.Unmarshal(rawMessage.Value, &message)
		// json.Unmarshal([]byte(rawMessage), &message)

		if message.Timestamp > previousMinute*60 && message.Timestamp < (previousMinute+1)*60 {
			var last = message.Id[0:numOfNibbles]
			if numOfNibbles%2 > 0 {
				// Decode string will simply omit the last 4 bit character if it is not paired
				last = "0" + last
			}
			num, err := hex.DecodeString(last)
			if err != nil {
				break
			}
			regId := bytesToRegId(num)
			var reg = message.Id[numOfNibbles:]
			i, err := hex.DecodeString(reg)
			numZeros := numOfLeadingZeros(i)
			if regs[Minute][regId] < numZeros {
				regs[Minute][regId] = numZeros
			}
		}

		var currentMinute int = message.Timestamp / 60

		if currentMinute > previousMinute {

			for i := 0; i < numOfIntervals-1; i++ {
				count[i] += 60
				if count[i] == getIntervalSecs(Interval(i)) {
					for j := 0; j < numOfBuckets; j++ {
						if regs[i+1][j] < regs[i][j] {
							regs[i+1][j] = regs[i][j]
						}
					}

					outputTime := time.Unix(int64(previousMinute*60), 0)
					estimation := estimate(regs[i], alphaM)

					r := OutputMessage{Timestamp: previousMinute * 60, Interval: Interval(i).String(), Hits: int(estimation)}
					go send(r)

					fmt.Printf("unique user estimation count for %s %v is  %v\n", Interval(i).String(), outputTime, estimation)

					count[i] = 0
					regs[i] = [numOfBuckets]int{}
				}
			}

		}

		previousMinute = currentMinute
	}

	if err := r.Close(); err != nil {
		log.Fatal("failed to close reader:", err)
	}

}

func getIntervalSecs(i Interval) int {
	switch i {
	case Minute:
		return 60
	case Hour:
		return 60 * 60
	case Day:
		return 24 * 60 * 60
	case Week:
		return 7 * 24 * 60 * 60
	}
	return 0
}

func estimate(reg [numOfBuckets]int, alphaM float64) int {
	result := 0.0
	reglen := len(reg)
	for index := 0; index < reglen; index++ {
		result += math.Pow(2, float64(-reg[index]))
	}
	estimation := math.Round(math.Pow(float64(reglen), 2) * alphaM * math.Pow(float64(result), -1.0))
	return int(estimation)
}

func bytesToRegId(arr []byte) uint32 {
	var len = len(arr)
	var val uint32 = 0
	for i := 0; i < len; i++ {
		val |= uint32(arr[i]) << (8 * (len - i - 1))
	}
	return val
}

func numOfLeadingZeros(arr []byte) int {
	total_res := 0
	for i := 0; i < len(arr); i++ {
		num := arr[i]
		res := 0
		for j := 0; j < 8; j++ {
			if num&0x80 == 0 {
				res += 1
				num <<= 1
			} else {
				break
			}
		}

		total_res += res
		if res < 8 {
			break
		}
	}

	return total_res + 1
}

func send(output OutputMessage) {

	w := &kafka.Writer{
		Addr:     kafka.TCP("localhost:9093"),
		Topic:    "output",
		Balancer: &kafka.LeastBytes{},
	}

	b, err := json.Marshal(output)
	if err != nil {
		fmt.Println(err)
		return
	}

	err2 := w.WriteMessages(context.Background(),
		kafka.Message{
			Value: []byte(b),
		},
	)
	if err2 != nil {
		log.Fatal("failed to write messages:", err2)
	}

	if err2 := w.Close(); err2 != nil {
		log.Fatal("failed to close writer:", err2)
	}
}
