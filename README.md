# Data Engineer Challenge Solution

To test the solution, you can start docker compose for kafka network: onto which the program connects:
~~~
docker-compose up -d
~~~

Download/ generate stream.jsonl file as input and you can then pipe it to kafka util which will act as a producer
~~~
cat stream.jsonl | docker-compose exec kafka bash -c 'kafka-console-producer.sh --broker-list localhost:9093 --topic data-challenge'
~~~

To process kafka messages:
~~~
cd hyperloglog && go build main.go && ./main
~~~

To read output data:
~~~
docker-compose exec -T kafka bash -c 'kafka-console-consumer.sh --topic output --from-beginning --bootstrap-server localhost:9093'
~~~

In this solution, hyperloglog algorithm/data structure is used, to aid in managing space complexity. It is an approximate result with a predictable error. The algorithm works by utilizing the fact that any bit in a uniformly distributed hash collection has 1/2 probability of being 0. Therefore, maximum number of consecutive 0's in a given hash collection approximates the number of unique elements. hyperloglog reduces the variance of the approximation by splitting said hashes in a number of buckets and then averages the result. This has the effect equivalent to using different hashings of the data, but with a reduced computational cost.

This solution specifically uses 12 bits of the hash for buckets, and the rest of the hash is used to count consecutive zeros. It is assumed that the uid is already hashed by a sufficiently good hashing algorithm, and re-computing the hash is skipped. The expected error can be calculated as e=1.04/sqrt(2^12), which is about 1.6%.

Given that the result is approximate, the streamed timestamps that come out-of-order are simply ignored. This could be improved if an additional bucket for the next minute is also maintained and the timestamp happens to fall in that period, but this increases memory needed, so it is not implemented.

Performance analysis is not done in detail, however, crude time measuring is done by varying the input size (switching from kafka to stdin to eliminate the network effects) and measuring execution time with varying number of input messages like so:

~~~
 time cat ../stream.jsonl | head -n 100000 | ./main
~~~

Results:

| Number of messages |   Time    |
|--------------------|-----------|
|       100000       | 0m1,933s  |
|       200000       | 0m3,818s  |
|       400000       | 0m7,611s  |
|      1000000       | 0m19,467s |


This shows that time complexity is roughly linear, as expected. However, changing the number of buckets from 4096 to 256 produces roughly the same time (roughly 19s), which means the performance is probably dominated by IO rather than computation and estimation. To properly benchmark and profile, the data could be preloaded into memory (optionally unmarshalled from json, since json is a potential bottle neck: suggested solution is with schema based serialization, such as protobuf) to further assess the performance of the solution.


Different time interval results are calculated by aggregating results from previous smaller interval results. This saves on computation, but has a limitation of those intervals being multiples of previous ones.

This solution is a per-frame one, meaning results are only computed for specific, discrete intervals. A sliding window solution with a similar, modified algorithm can also be achieved with very little additional memory increase, which can then enable the asynchronous retrieving of the results in arbitrary points in time.

This particular challenge has many corner cases, most of which aren't covered due to time constraints. The same is true for optimizations and quality of life and code improvements.

Overall, this has been a fun challenge and has been challenging in different aspects.